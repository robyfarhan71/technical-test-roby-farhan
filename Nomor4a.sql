-- 1. Tampilkan seluruh data dari table book : --
	Select * from book_tb;

-- 2. Tampilkan seluruh data book, category, penulis : --
	Select * from book_tb INNER JOIN category_tb ON book_tb.category_id=category_tb.category_id 
	INNER JOIN writer_tb on book_tb.writer_id = writer_tb.writer_id;

-- 3. Tampilkan seluruh data penulis : --
	Select * from writer_tb;
	
-- 4. Tampilkan spesifik book beserta category maupun penulis --
	Select * from book_tb INNER JOIN category_tb ON book_tb.category_id=category_tb.category_id 
	INNER JOIN writer_tb on book_tb.writer_id = writer_tb.writer_id WHERE writer_tb.name = 'Setiawan';
	
-- 5. Screen shotkan juga hasil query POST/menambah data --